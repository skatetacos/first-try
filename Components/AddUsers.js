import React from 'react'
import { FlatList, Text, View, TextInput, Button, StyleSheet } from 'react-native'

import user from '../src/data/UserData'

class AddUsers extends React.Component {

  constructor(props) {
      super(props)
      this.state = {

        UserData:[{
          id:0,
          pseudo:'',
          card01:0,
          card02:0,
          card03:0,
          card04:0}],

        pseudoText:""
      }
  }
// Fonction texte du placeholder
  _pseudoTextInputChange(text){
    this.setState[{pseudoText:text}]
  }
// Fonction pour ajout user
  _addUser(pseudoText){
    this.setState[{}]
  }


 // Affichage
  render() {
    return (
      <View style={styles.main_container}>
        <FlatList
          style={styles.flatList}
          data={user}
          keyExtractor={(item) => item.pseudo.toString()}
          renderItem={({item}) => <Text style={styles.textList}>{item.pseudo}</Text>}
        />
        <TextInput
          style={styles.textInput}
          placeholder='Ajouter Joueur'
          onChangeText={(text) => this._pseudoTextInputChange(text)}
        />
        <Button
          style={styles.button}
          title='Add'
          onPress={(pseudoText => {this._addUser(pseudoText)})}
        />
      </View>

        )
    }
}
// Feuille des styles
const styles = StyleSheet.create ({
  main_container :{
    backgroundColor:'#2E4053',
    flex:1,
    justifyContent:'center'

  },

  textInput:{
    backgroundColor:'#616A6B',
    flex:1,
    textAlign:'center',
    maxHeight:50,
    fontSize:20,
    borderColor:'black'

  },

  button:{
    flex:1

  },

  flatList:{
    paddingTop:50,
    textAlign:'center',
  },

  textList:{
    color:'white',
    fontSize:30,
    textAlign:'center',

  }


})

export default AddUsers
